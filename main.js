window.addEventListener('load', function(){
    const main = document.getElementById('app-container');
    const footer = document.querySelector('.app-footer');
    const section = document.createElement('section');
    section.classList.add("app-section", "app-section--image-program");
    const h2 = document.createElement('h2');
    h2.classList.add('app-title');
    h2.textContent = 'Join Our Program';
    const h3 = document.createElement('h3');
    h3.classList.add('app-subtitle');
    h3.innerHTML = `Sed do eiusmod tempor incididunt <br>
    ut labore et dolore magna aliqua.`;
    const form = document.createElement('form');
    form.setAttribute('id', 'myForm');
    const input = document.createElement('input');
    input.setAttribute('type', 'email');
    input.setAttribute('name', 'email');
    input.setAttribute('id', 'email');
    input.setAttribute('placeholder', 'Email');
    const button = document.createElement('button');
    button.classList.add("app-section__button", "app-section__button--subscire")
    button.textContent = 'Subscribe';




    main.insertBefore(section, footer);
    section.appendChild(h2);
    section.appendChild(h3);
    section.appendChild(form);
    form.appendChild(input);
    form.appendChild(button);


    button.addEventListener('click', function(){
        form.addEventListener('submit', function(e){
            e.preventDefault();
            
        });
        console.log(input.value);
        input.value ="";
    })
});

